# DDNS Infomaniak Update

## Requirements

- Having an existing domain hosted by Infomaniak
- Create a dedicated DDNS username and password
- Configure variables in the file `ddns.conf`
- Schedule the script to run every 20 minutes

This script is not provided nor promoted by Infomaniak.

More infomation at https://faq.infomaniak.com/2376
