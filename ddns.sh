#!/usr/bin/env bash
#
# Author: Maurice Kaag <maurice@kaag.me>
# Licence: BSD-3-Clause
# Usage: ./ddns.sh
#
# Name: DDNS Infomaniak Update (ddns-ik-update)
# Description: the script detects a change of the public IP since last run, which then trigger an update of the DDNS record.
# Requirements: Having a domain hosted at Infomaniak. Create a dedicated DDNS username and password.
# Crontab (user): run the script every 20 minutes.
#
# # Bash Settings
set -o errexit   # abort on nonzero exitstatus
set -o nounset   # abort on unbound variable
set -o pipefail  # don't hide errors within pipes

# Bash Variables
readonly script_name=$(basename "${0}")
readonly script_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
IFS=$'\t\n'   # Split on newlines and tabs (but not on spaces)

# Program Variables
source "${script_dir}/ddns.conf"
readonly CURRENT_IP_FILE="${script_dir}/ip.current" # File storing the current IP address, used to detect changes.
OLD_IP=""                             # Ephemeral variable for the IP stored in the CURRENT_IP_FILE
NEW_IP=""                             # Ephemeral variable for the IP obtained from remove website

# Handle the closing of the program.
# Params: exit code from previous command
finish() {
  result=$?
  exit ${result}
}

# Save the new IP address into a local file for next check.
# Params: filename and IP address
function updateCurrentFile {
  local _file="$1"
  local _ip="$2"
  echo "Update the file ${_file} with IP ${_ip}"
  echo ${_ip} > ${_file}
}

# Update the DDNS record
# Params: none
function updateRecord {
  echo "Update DDNS entry at Infomaniak with ${NEW_IP}"
  curl -X POST -F "hostname=${DDNS_RECORD}" -F "username=${DDNS_USERNAME}" -F "password=${DDNS_PASSWORD}" ${DDNS_UPDATE_URL}
  echo ""
}

# Main function
# Params: none
main() {
  # Do we have an existing entry?
  if [ -f ${CURRENT_IP_FILE} ]; then
    echo "Found the file ${CURRENT_IP_FILE}"
    OLD_IP=`cat ${CURRENT_IP_FILE}`
  else
    echo "Cannot find the file ${CURRENT_IP_FILE}"
    touch ${CURRENT_IP_FILE}
  fi

  # Lets compare the current IP with the new one
  NEW_IP=`curl --silent https://ifconfig.me`
  echo New IP address: ${NEW_IP}

  if [ "${OLD_IP}" != "" ]; then
    echo "Old IP address: ${OLD_IP}"
    if [ "${OLD_IP}" == "${NEW_IP}" ]; then
      echo "IPs do match, nothing to do"
    else
      echo "IPs mismatch"
      updateCurrentFile ${CURRENT_IP_FILE} ${NEW_IP}
      updateRecord
    fi
  else
    echo "Cannot find an IP address in ${CURRENT_IP_FILE}"
    updateCurrentFile ${CURRENT_IP_FILE} ${NEW_IP}
    updateRecord
  fi
}

# Program
# Params: none
main
trap finish EXIT ERR